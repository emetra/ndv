CREATE PROCEDURE NDV.GetType34WithoutConsent( @StudyId INT ) AS
BEGIN
  SET NOCOUNT ON;
  SELECT v.*,'Ukjent/ubesvart samtykke' AS InfoText
  FROM dbo.GetLastEnumValues( 3196, NULL ) DiaType 
  LEFT JOIN dbo.GetLastEnumValuesTable( 3389, NULL ) Samtykke ON Samtykke.PersonId = DiaType.PersonId
  JOIN dbo.ViewActiveCaseListStub v ON v.PersonId = DiaType.PersonId AND v.StudyId=@StudyId
  WHERE ( DiaType.EnumVal IN (3,4) ) AND ( NOT ISNULL(Samtykke.EnumVal,-1) IN (1,2,3) )
END