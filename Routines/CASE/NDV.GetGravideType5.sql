CREATE PROCEDURE NDV.GetGravideType5( @StudyId INT ) AS
BEGIN         
  EXEC NDV.GetPregnantByType @StudyId, 5;
END