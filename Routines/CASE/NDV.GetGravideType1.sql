CREATE PROCEDURE NDV.GetGravideType1 ( @StudyId INT )
AS
BEGIN
  EXEC NDV.GetPregnantByType @StudyId, 1
END
