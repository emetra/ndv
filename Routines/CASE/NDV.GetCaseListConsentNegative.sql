CREATE PROCEDURE NDV.GetCaseListConsentNegative( @StudyId INT )
AS
BEGIN
  -- Finn diabetikere uansett StudyId
  SELECT DISTINCT ce.PersonId 
  INTO #t1 
  FROM dbo.ClinEvent ce
  JOIN dbo.ClinDataPoint cdp ON cdp.EventId = ce.EventId AND cdp.ItemId = 3196 AND cdp.EnumVal > 0;
  -- Finn samtykkestatus
  SELECT t1.PersonId,ISNULL(dbo.GetLastEnumVal(t1.PersonId,'NDV_CONSENT'),-1) AS NDV_CONSENT
  INTO #t2
  FROM #t1 t1;
  -- Lag resultatsett
  SELECT t2.PersonId, v.DOB, v.FullName, v.GroupName,
    CASE NDV_CONSENT 
      WHEN 2 THEN 'NDV Ikke samtykke' 
      WHEN 3 THEN 'NDV Trukket samtykke' 
    END AS InfoText
  FROM dbo.ViewActiveCaseListStub v 
  JOIN #t2 t2 ON t2.PersonId = v.PersonId AND v.StudyId=@StudyId
  WHERE NDV_CONSENT IN (2,3);
END