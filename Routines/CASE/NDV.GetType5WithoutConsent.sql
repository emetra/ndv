CREATE PROCEDURE NDV.GetType5WithoutConsent( @StudyId INT ) AS
BEGIN
  SET NOCOUNT ON;
  SELECT v.*,'Ukjent/ubesvart samtykke' AS InfoText
  FROM dbo.GetLastEnumValuesTable( 3196, NULL ) DiaType 
  LEFT JOIN dbo.GetLastEnumValuesTable( 3389, NULL ) Samtykke ON Samtykke.PersonId = DiaType.PersonId
  JOIN dbo.ViewActiveCaseListStub v ON v.PersonId = DiaType.PersonId AND v.StudyId=@StudyId
  WHERE ( DiaType.EnumVal = 5 ) AND ( NOT ISNULL(Samtykke.EnumVal,-1) IN (1,2,3) )
END