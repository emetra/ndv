CREATE PROCEDURE NDV.GetGravideType2 ( @StudyId INT )
AS
BEGIN
  EXEC NDV.GetPregnantByType @StudyId, 2
END
