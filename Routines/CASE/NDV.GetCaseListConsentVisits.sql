CREATE PROCEDURE NDV.GetCaseListConsentVisits( @StudyId INT, @StartDate DateTime, @StopDate DateTime ) AS
BEGIN
  SELECT a.PersonId, p.DOB, p.ReverseName AS FullName, a.GroupName, 
  'Fant ' + CONVERT(VARCHAR,n) + ' kontakt(er) i perioden.' AS InfoText
  FROM 
  (
    SELECT sam.PersonId, sg.GroupName, count(*) AS n
    FROM dbo.GetLastEnumValuesTable( 3389, NULL ) Sam
    JOIN dbo.ClinEvent ce ON ce.PersonId = Sam.PersonId 
    JOIN dbo.StudyGroup sg ON sg.StudyId = ce.StudyId AND sg.GroupId = ce.GroupId
    JOIN dbo.UserList ul ON ul.UserId = USER_ID() AND ul.CenterId = sg.CenterId
    WHERE Sam.EnumVal = 1 AND ce.EventTime BETWEEN @StartDate AND @StopDate
    GROUP BY sam.PersonId, sg.GroupName
   ) a
  JOIN dbo.Person p ON p.PersonId = a.PersonId
  ORDER BY p.ReverseName;
END;