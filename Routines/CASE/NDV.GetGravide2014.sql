CREATE PROCEDURE NDV.GetGravide2014(  @StudyId INT )
AS
BEGIN
  EXEC NDV.GetPregnantByYear @StudyId,2014
END