CREATE PROCEDURE NDV.GetCaseListConsentUnknown( @StudyId INT )
AS
BEGIN
  -- TODO: Kan forbedres med GetLastEnumValTable
  -- Finn diabetikere uansett StudyId
  SELECT DISTINCT ce.PersonId 
  INTO #t1 
  FROM dbo.ClinEvent ce
  JOIN dbo.ClinDataPoint cdp ON cdp.EventId = ce.EventId AND cdp.ItemId = 3196 AND cdp.EnumVal > 0;
  -- Finn samtykkestatus
  SELECT t1.PersonId,ISNULL( dbo.GetLastEnumVal( t1.PersonId, 'NDV_CONSENT' ), -1 ) AS NDV_CONSENT
  INTO #t2
  FROM #t1 t1;
  -- Lag resultatsett
  SELECT t2.PersonId, v.DOB, v.FullName, v.GroupName,
    CASE NDV_CONSENT 
      WHEN -1 THEN 'NDV Samtykke ubesvart' 
      WHEN 4 THEN 'NDV Samtykke ukjent' 
    END AS InfoText
  FROM dbo.ViewActiveCaseListStub v 
  JOIN #t2 t2 ON t2.PersonId = v.PersonId AND v.StudyId = @StudyId
  WHERE NDV_CONSENT IN (-1,4);
END