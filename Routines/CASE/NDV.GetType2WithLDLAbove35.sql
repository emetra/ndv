CREATE PROCEDURE NDV.GetType2WithLDLAbove35( @StudyId INT ) AS 
BEGIN
  EXECUTE NDV.GetType2WithHighLDL @StudyId,3.51
END