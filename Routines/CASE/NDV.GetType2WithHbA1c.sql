CREATE PROCEDURE NDV.GetType2WithHbA1c( @StudyId INT ) AS
BEGIN
  SELECT p.PersonId,p.DOB,p.FullName,p.GroupName,
    COALESCE(ld.LabName + ' = ' + CONVERT(VARCHAR,ld.NumResult ) +' (' +
    CONVERT(VARCHAR,DATEPART(YYYY,ld.LabDate)) + ')','(ikke m�lt)') AS InfoText
  FROM NDV.Type2 p
  LEFT JOIN dbo.GetLastLabDataTable( 1058, '3000-01-01' ) ld ON ld.PersonId= p.PersonId
  ORDER BY ISNULL(ld.NumResult,999) DESC
END