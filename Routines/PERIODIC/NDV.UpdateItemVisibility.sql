CREATE PROCEDURE NDV.UpdateItemVisibility AS
BEGIN
  SET NOCOUNT ON;
  IF DB_NAME() = 'EFT00048' -- AHUS
  BEGIN
   -- Har et prosjekt for digital oppf�lgning, og bruker variable for � holde orden p� dette her:
    UPDATE dbo.MetaFormItem SET Visibility = 0 
    WHERE 
      ItemId IN ( 12749, 12755 ) AND 
      FormId IN ( SELECT FormId FROM dbo.MetaForm WHERE FormName IN ( 'DIAPOL_GRAVIDE', 'DIAPOL_MAIN', 'DIAPOL_YEAR' ) );
  END;          
  IF DB_NAME() = 'EFT00028' -- Helse Bergen 
  BEGIN
    UPDATE dbo.MetaStudyForm SET SurveyStatus = 'Open' WHERE FormId IN ( 1360, 1361 );
    UPDATE dbo.MetaFormItem SET Visibility = 1 WHERE ItemId IN ( 5830, 5831, 5832 );  
  END;
  IF DEFAULT_DOMAIN() = 'HS' -- Helse Vest
  BEGIN
    UPDATE dbo.MetaFormItem SET Visibility = 1 WHERE ItemId = 1502; 
  END;
END