CREATE PROCEDURE Dash.NdvType1HbA1cBelow52 AS
BEGIN
  EXEC Dash.NdvType1HbA1cBelow 52, 15, 4;
END