Om NDV
======

NDV er en forkortelse for "Norsk Diabetesregister for Voksne". Her finner du dokumenter som er spesifikke for NDV. Disse filene brukes i FastTrak på diabetespoliklinikker i hele Norge.

Mapper
======

* **Documents** - innholder dokumenter som ikke er brukt direkte av systemet.
* **Images** - Inneholder bilder som inngår i dokumentasjon.
* **Patient** - Rapporter (\*.fr3) og oversiktsbilde (\*.html) for en enkeltpasient, samt PDF-filer som brukes av systemet og notatmaler (\*.note).
* **Population** - Rapporter (*.fr3) som inneholder flere pasienter.

Nedlasting
==========

Forvaltere kan laste ned ukentlige oppdateringer av metadata, og kan også få tilsendt en epost med instruksjoner og lenke til nedlasting av zip-fil. 
Ta kontakt med DIPS Brukerstøtte for mer informasjon.


Installasjon
============

Pakk ut nedlastet zip-fil og kopier inn med overskriving av eksisterende filer. 
Kjør deretter `\bin\FastTrakUpdate.exe` og kryss av for alle tabeller før kjøring. 
Husk å krysse av for "Lokale filer".

Andre ressurser
===============
I tillegg til disse filene består protokollen av skjema, populasjoner, rapporter og beslutningsstøtte.

* Populasjoner, rapporter og beslutningsstøtte som er spesifikke for denne protokollen er listet opp her: https://fasttrak.dips.no/CRFShowPopulations.asp?StudyName=NDV
* Alle skjema som inngår er ligger her: https://fasttrak.dips.no/CRFShowStudy.asp?StudyName=NDV


Bergen, 19. februar 2019

Magne Rekdal